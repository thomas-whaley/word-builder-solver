def get_letters():
    # Get input
    # LATER: bounds checking
    #   Eg: no numbers or weird characters
    return input(">>> ").lower()


def make_words_list(min_length, max_length):
    # Return all words that have a length greater than min_length and less than max_length
    with open("words_alpha.txt", "r") as file:
        return [word[:-1].lower() for word in file.readlines() if max_length > len(word[:-1]) >= min_length]


def can_we_make_it(w, l):
    # Can we make the word using the given letters?
    # Is the length of the word greater than the number of letters?
    if len(w := [x for x in w]) > len(l := [x for x in l]): return False
    # Is the word is the same as the letters?
    if w == l: return True
    # Get the first letter of the word and check if it is in the letter list
    while len(w) > 0:
        if w[0] in l:
            l.remove(w.pop(0))  # The letter is in the letter list
        else:
            return False  # The letter isn't in the letter list: AKA BIG BAD
    return True


def get_words_possible(min, max, letters, required_letter):
    # Returns a list of all possible words that you can make using the letter lists
    # All words must have the required_letter in them
    return [x for x in [x for x in make_words_list(min, max) if can_we_make_it(x, letters)] if required_letter in x]


while True:
    min_length = 3
    print("Letters:")
    my_letters = get_letters()
    print("Required letter (if none leave blank):")
    required_letter = get_letters()
    import time

    t1 = time.time()
    possible_word_list = get_words_possible(min_length, len(my_letters), my_letters, required_letter)
    t2 = time.time() - t1
    if len(possible_word_list) > 0:
        print(f"\nWords possible ({len(possible_word_list)} words):")
        for word in possible_word_list:
            print(f"  -{word}")
        print(f"\nProcess took {t2} second(s).")
    else:
        print("No possible words")
    print("------")