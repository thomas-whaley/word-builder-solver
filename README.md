# Word Builder Solver

## Usage


Example: we are given the letters `face`

```
Letters:
>>> face
Required letter (if none leave blank):
>>> 

Words possible (5 words):
  -ace
  -caf
  -fac
  -fae
  -fec

Process took 0.09482693672180176 second(s).
------
```

This shows that we can make these 5 words (debatable if they are actually words or not)